const db = uniCloud.database();
const redis = uniCloud.redis();
module.exports = {
	_before: function () { // 通用预处理器

	},
	async demo(){
		// let res = await redis.hset("redisDemo:demo-hash","username",JSON.stringify({name:"咸虾米",age:18}))
		// return res;
		
		// let res = await redis.hget("redisDemo:demo-hash","username");
		// return res;
		// await redis.hmset("redisDemo:demo-hash","张三","18","李四","22")
		// return await redis.hmget("redisDemo:demo-hash","张三","李四")
		// return await redis.hkeys("redisDemo:demo-hash");
		return await redis.hlen("redisDemo:demo-hash");
	}
}
