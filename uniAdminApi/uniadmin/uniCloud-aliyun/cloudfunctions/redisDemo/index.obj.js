const db =  uniCloud.database();
const redis = uniCloud.redis();
module.exports = {
	_before: function () { // 通用预处理器
		this.startTime = Date.now();
	},
	async getNavList(){
		let redisGet = await redis.get("redisDemo:navList");
		let res={};
		if(redisGet){
			res.data = JSON.parse(redisGet);
		}else{
			res = await db.collection("xzs_product_nav").orderBy("orderid","desc").field({
				state:true,
				classname:true,
				orderid:true,
				"icon.url":true
			}).get();
			await redis.set("redisDemo:navList",JSON.stringify(res.data),'EX', 600);
		}
		return {data:res.data,code:200,msg:"查询成功"};
	},
	_after:function(error,result){
		if(error){
			throw error;
		}
		result.timeCost = Date.now() - this.startTime;
		return result;
	}
	
}
