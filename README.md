# uniapp及unicloud学习路径

> uniapp零基础入门，到项目案例开发的所有源码在此，大家根据相关目录结构找到对应的项目，每个目录下面都提供了配套的学习资料，完全开源免费，对你起到作用的话，请给个star吧，代码不断升级项目案例不断开发，请大家持续关注，也可以进入QQ群，有新的项目，会在群里提醒的，Q群：770479352。

![咸虾米的Q群](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/0b7fa2fa-06ed-4aa5-bca7-b3f29157a58f.png)

------



## 一、uniapp零基础入门

{2022新课uniapp零基础入门到项目打包（微信小程序/H5/vue/安卓apk）全掌握}

#### 视频学习地址：https://www.bilibili.com/video/BV1mT411K7nW

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/b74295a6-872c-4321-a8c9-1fa8ba41add8.jpg)

### 配套案例（青年帮新闻）

#### 扫码体验项目↓

<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/2624cc01-bd6c-470f-8c61-2d493bd56f21.png" style="width:180px"/>

#### 项目演示地址：https://ku.qingnian8.com/case/qingnews
#### 素材仓库地址：https://gitee.com/qingnian8/uniall/tree/master/uniappBase



------



## 二、uniCloud基础入门到项目开发

{uniCloud云开发视频教程-从基础入门到项目开发实战}

#### 视频学习地址：https://www.bilibili.com/video/BV1PP411E7qG

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/b422126b-d805-4a92-87f0-30624531cf53.jpg)

### 配套案例（咸虾米的文章），文章管理系统

#### 扫码体验项目 ↓

<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/937eecff-35fc-4ce0-a931-d6e0347a5ab0.png"  style="width:180px"/>

#### 项目演示地址：http://ku.qingnian8.com/case/uniArticle

#### 素材仓库地址：https://gitee.com/qingnian8/uniall/tree/master/uniCloudBase



------


## 三、uniCloud项目进阶开发-咸虾米的圈子社区案例

{uniCloud云开发进阶篇多用户社区博客实战项目开发教程（Schema/JQL语法/uni-id用户/uview-ui/uniapp/uni-admin）}

视频学习地址：https://www.bilibili.com/video/BV1yG4y1h7ck

![输入图片说明](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/f98ed7db-dab8-4299-88cc-e8f76de4dac9.png)

### 配套案例演示及素材资料 

#### 扫描下方二维码，体验项目↓

<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-3309c116-4743-47d6-9979-462d2edf878c/721ac764-4f9c-45f9-9686-91214087c5b5.png"  style="width:180px"/>

#### 项目演示地址：https://static-mp-3309c116-4743-47d6-9979-462d2edf878c.next.bspapp.com/quanzi/index.html

#### 素材仓库地址：https://gitee.com/qingnian8/uniall/tree/master/uniCloudRise

------

## 四、基于uniapp生态开发API接口

{基于uni-app生态开发API接口，uniadmin/uniapp/unicloud进阶课}

视频学习地址：https://www.bilibili.com/video/BV1Xe4y1V7Lw

![](https://mp-3309c116-4743-47d6-9979-462d2edf878c.cdn.bspapp.com/cloudstorage/ccc0d02c-9aba-41c7-a197-1b198c0a5f4c.jpg)

#### API在线接口文档：https://console-docs.apipost.cn/preview/8b23f100b39a63c5/617499421662264b

#### 素材仓库地址：https://gitee.com/qingnian8/uniall/tree/master/uniAdminApi

------

## 五、uniPush统一推送与App升级中心uni-upgrade-center

{uniapp系列进阶课，安卓apk打包及自定义基座，实现APP消息推送及APP软件升级包服务}

视频学习地址：https://www.bilibili.com/video/BV1Bk4y187MV

![](https://mp-3309c116-4743-47d6-9979-462d2edf878c.cdn.bspapp.com/cloudstorage/f0b4903d-6ec0-4b23-aeec-690505f46eb7.jpg)

#### 项目演示：请到学习视频中查看

#### 仓库地址：无

------

## 六、uni-pay支付商城项目开发

{自建微信小程序购物商店项目实战，uniapp及uniCloud云开发进阶课}

视频学习地址：https://www.bilibili.com/video/BV1ts4y1B7rp

![](https://mp-3309c116-4743-47d6-9979-462d2edf878c.cdn.bspapp.com/cloudstorage/98730f92-8fb8-40a2-b9e9-896af4a0504e.jpg)

#### 微信扫码体验小程序项目 ↓
![](https://mp-3309c116-4743-47d6-9979-462d2edf878c.cdn.bspapp.com/xxmPath/xxm-mp-code.jpg)

#### H5项目演示地址：https://static-mp-3309c116-4743-47d6-9979-462d2edf878c.next.bspapp.com/xxmBigMall/index.html#/

#### 仓库地址：https://gitee.com/qingnian8/XXMuniPayMall

------

